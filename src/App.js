import "./App.css";
import React from "react";
import CreateNote from "./Components/CreateNote";
import Footer from "./Components/Footer";
import Header from "./Components/Header";
// import Note from "./Components/Note";

function App() {
  return (
    <>
      <Header />
      <CreateNote />
      <Footer />
    </>
  );
}

export default App;
