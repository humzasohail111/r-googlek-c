import React from "react";

const Footer = () => {
  const year = new Date().getFullYear();

  return (
    <>
      <footer className="page-footer font-small red">
        <div className="footer-copyright text-center py-3">
          © {year} Copyright: Google Keep
        </div>
      </footer>
    </>
  );
};

export default Footer;
