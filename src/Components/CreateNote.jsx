import React, { useState } from "react";
import AddIcon from "@material-ui/icons/Add";
import "../Style/CN.css";
import Note from "./Note";

const CreateNote = () => {
  const [note, setNote] = useState({
    title: "",
    content: "",
  });

  const [noteItem, setNoteItem] = useState([]);

  const Show = (event) => {
    const { name, value } = event.target;

    setNote((preValue) => {
      return {
        ...preValue,
        [name]: value,
      };
    });
  };

  const addNote = () => {
    setNoteItem((preValue) => {
      return [...preValue, note];
    });
  };

  const onDelete = (id) => {
    setNoteItem((preValue) => {
      return preValue.filter((indexElement, index) => {
        return index !== id;
      });
    });
  };

  return (
    <>
      <div className="main-card">
        <div className="card">
          <div className="container">
            <form>
              <input
                type="text"
                className="cnInput"
                placeholder="Title"
                onChange={Show}
                value={note.title}
                name="title"
              />
              <br />
              <textarea
                className="cnTextArea"
                rows=""
                column=""
                placeholder="Write a note..."
                onChange={Show}
                value={note.content}
                name="content"
              />
              <br />
              <button className="buttonC" onClick={addNote} type="button">
                <AddIcon className="cnAddIcon" />
              </button>
            </form>
          </div>
        </div>
      </div>

      <div className="main-cardN">
        {noteItem.map((itemIndex, index) => {
          return (
            <Note
              key={index}
              id={index}
              title={itemIndex.title}
              content={itemIndex.content}
              onSelect={onDelete}
            />
          );
        })}
      </div>
    </>
  );
};

export default CreateNote;
