import React, { useState } from "react";

const CForm = () => {
  const [FullName, setFullName] = useState({
    FName: "",
    LName: "",
    Email: "",
    Number: "",
  });

  const inputEvent = (event) => {
    const { value, name } = event.target;

    setFullName((preValue) => {
      return {
        ...preValue,
        [name]: value,
      };
      // if (name === "FName") {
      //   return {
      //     FName: value,
      //     LName: preValue.LName,
      //     Email: preValue.Email,
      //     Number: preValue.Number,
      //   };
      // } else if (name === "LName") {
      //   return {
      //     FName: preValue.FName,
      //     LName: value,
      //     Email: preValue.Email,
      //     Number: preValue.Number,
      //   };
      // } else if (name === "Email") {
      //   return {
      //     FName: preValue.FName,
      //     LName: preValue.LName,
      //     Email: value,
      //     Number: preValue.Number,
      //   };
      // } else if (name === "Number") {
      //   return {
      //     FName: preValue.FName,
      //     LName: preValue.LName,
      //     Email: preValue.Email,
      //     Number: value,
      //   };
      // }
    });
  };

  const onSubmit = (event) => {
    event.preventDefault();
    alert("Form Submitted!");
  };

  return (
    <>
      <form onSubmit={onSubmit}>
        <div className="button-div">
          <h1>
            Hello {FullName.FName} {FullName.LName}
          </h1>
          <p>{FullName.Email}</p>
          <p>{FullName.Number}</p>
          <input
            type="text"
            placeholder="Enter your First Name!"
            name="FName"
            onChange={inputEvent}
            value={FullName.FName}
          />
          <input
            type="text"
            placeholder="Enter your Second Name!"
            name="LName"
            onChange={inputEvent}
            value={FullName.LName}
          />
          <input
            type="text"
            placeholder="Enter your Email!"
            name="Email"
            onChange={inputEvent}
            value={FullName.Email}
          />
          <input
            type="number"
            placeholder="Enter your Number!"
            name="Number"
            onChange={inputEvent}
            value={FullName.Number}
          />

          <button type="submit">Click Me! 👍</button>
        </div>
      </form>
    </>
  );
};

export default CForm;
