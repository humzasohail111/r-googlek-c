import React, { useState } from "react";

const Form = () => {
  const [fullName, setFullName] = useState({
    fName: "",
    lName: "",
  });

  const inputEvent = (event) => {
    const value = event.target.value;
    const name = event.target.name;

    setFullName((preValue) => {
      if (name === "fName") {
        return {
          fName: value,
          lName: preValue.lName,
        };
      } else if (name === "lName") {
        return {
          fName: preValue.fName,
          lName: value,
        };
      }
    });
  };

  const onSubmit = (event) => {
    event.preventDefault();
    alert("Form Submitted!");
  };

  return (
    <>
      <div className="button-div">
        <form onSubmit={onSubmit}>
          <h1>
            Hello {fullName.fName} {fullName.lName}
          </h1>
          <input
            type="text"
            placeholder="Enter your name!"
            name="fName"
            onChange={inputEvent}
            value={fullName.fName}
          />
          <input
            type="text"
            placeholder="Enter your password!"
            name="lName"
            onChange={inputEvent}
            value={fullName.lName}
          />
          <button type="submit">Click Me! 👍</button>
        </form>
      </div>
    </>
  );
};

export default Form;
