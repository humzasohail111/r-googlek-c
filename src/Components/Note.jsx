import React from "react";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";

import "../Style/Note.css";

const Note = (props) => {
  // const deleteNode = () => {
  //   props.deleteItem(props.id);
  // };

  return (
    <>
      <div className="cardN">
        <div className="containerN">
          <form>
            <h6 className="NTitle"> {props.title} </h6>
            <br />
            <p className="NText">{props.textNote}</p>
          </form>
          <button
            className="buttonN"
            onClick={() => {
              props.onSelect(props.id);
            }}
          >
            <DeleteOutlineIcon />
          </button>
        </div>
      </div>
    </>
  );
};

export default Note;
